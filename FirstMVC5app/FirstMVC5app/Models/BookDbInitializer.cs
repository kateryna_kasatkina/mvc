﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FirstMVC5app.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext db)
        {
            db.Books.Add(new Book { Name = "Harry Potter 1", Author = "D.K.Rowling", Price = 1000 });
            db.Books.Add(new Book { Name = "Harry Potter 2", Author = "D.K.Rowling", Price = 1000 });
            db.Books.Add(new Book {Name = "Harry Potter 3", Author = "D.K.Rowling", Price = 1000 });
            db.Books.Add(new Book { Name = "Harry Potter 4", Author = "D.K.Rowling", Price = 1000 });
            db.Books.Add(new Book { Name = "Harry Potter 5", Author = "D.K.Rowling", Price = 1000 });
            db.Books.Add(new Book { Name = "Harry Potter 6", Author = "D.K.Rowling", Price = 1000 });
            base.Seed(db);
        }
    }
}