﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FirstMVC5app.Startup))]
namespace FirstMVC5app
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
