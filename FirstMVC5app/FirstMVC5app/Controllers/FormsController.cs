﻿using FirstMVC5app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMVC5app.Controllers
{
    public class FormsController : Controller
    {
        BookContext db = new BookContext();

        // GET: Forms
        public ActionResult Index()
        {
            IEnumerable<Book> model = db.Books;
            return View(model);
        }

        public ActionResult MyPurchase(int id=1)
        {
            //IEnumerable<Purchase> model = db.Purchases;
            //return View(model);

            Purchase model = db.Purchases.Where(p => p.PurchaseId == id).FirstOrDefault();
            return View(model);
        }

        public ActionResult MyDropDownList()
        {
            SelectList books = new SelectList(db.Books, "Author", "Name");
            ViewBag.Books = books;
            return View();
        }

        [HttpPost]
        public string Index(string[] fruits)
        {
            string result = "";
            foreach (string c in fruits)
            {
                result += c;
                result += ";";
            }
            return "Вы выбрали: " + result;
        }

      
    }
}