﻿using FirstMVC5app.Models;
using FirstMVC5app.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMVC5app.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        BookContext db = new BookContext();


        //public string Index()
        //{
        //    string browser = HttpContext.Request.Browser.Browser;
        //    string user_agent = HttpContext.Request.UserAgent;
        //    string url = HttpContext.Request.RawUrl;
        //    string ip = HttpContext.Request.UserHostAddress;
        //    string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
        //    return "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
        //        "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        //}

        public string ContextData()
        {
            HttpContext.Response.Write("<h1>Hello World</h1>");

            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        }

        public ActionResult Index()
        {
            // получаем из бд все объекты Book
            IEnumerable<Book> books = db.Books;
            // передаем все объекты в динамическое свойство Books в ViewBag
            ViewBag.Books = books;
            // возвращаем представление
            return View();
        }

        public ViewResult MyIndex()
        {
            IEnumerable<Book> model = db.Books;
            return View(model);
        }

        [HttpGet]
        public ActionResult Buy(int id=1)
        {
            ViewBag.BookId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;
            db.Purchases.Add(purchase);
            db.SaveChanges();
            return "Thanks, " + purchase.Person + " , for purchase!";
        }


        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Привет мир!</h2>");
        }

        public ActionResult GetImage()
        {
            string path = "../Images/bullet_blue.png";
            return new ImageResult(path);
        }


        public FileResult GetFile()
        {
            // Путь к файлу
            string file_path = @"D:\MVC4\FirstMVC5app\FirstMVC5app\Files\The Great Gatsby.pdf";
            // Тип файла - content-type
            string file_type = "application/pdf";
            // Имя файла - необязательно
            string file_name = "The Great Gatsby.pdf";
            return File(file_path, file_type, file_name);
        }

        // Отправка массива байтов
        public FileResult GetBytes()
        {
            string path = Server.MapPath("~/Files/The Great Gatsby.pdf");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/pdf";
            string file_name = "The Great Gatsby.pdf";
            return File(mas, file_type, file_name);
        }


        // Отправка потока
        public FileResult GetStream()
        {
            string path = Server.MapPath("~/Files/The Great Gatsby.pdf");
            // Объект Stream
            FileStream fs = new FileStream(path, FileMode.Open);
            string file_type = "application/pdf";
            string file_name = "The Great Gatsby.pdf";
            return File(fs, file_type, file_name);
        }

        public string Square(int a, int h)
        {
            double s = a * h / 2.0;
            return "<h2>Площадь треугольника с основанием " + a +
                    " и высотой " + h + " равна " + s + "</h2>";
        }

        public RedirectToRouteResult SomeMethod()
        {
            return RedirectToAction("Square", "Home", new { a = 10, h = 12 });
        }

        //public string Square(int a = 10, int h = 3)
        //{
        //    double s = a * h / 2.0;
        //    return "<h2>Площадь треугольника с основанием " + a +
        //            " и высотой " + h + " равна " + s + "</h2>";
        //}


        //public string Square()
        //{
        //    int a = Int32.Parse(Request.Params["a"]);
        //    int h = Int32.Parse(Request.Params["h"]);
        //    double s = a * h / 2.0;
        //    return "<h2>Площадь треугольника с основанием " + a + " и высотой " + h + " равна " + s + "</h2>";
        //}
    }
}