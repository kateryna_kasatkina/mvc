﻿using System.Data.Entity;

namespace TTTMVC.Models
{
    public class GameContext: DbContext
    {
        public DbSet<Cell> field { get; set; }
        public DbSet<Player> players { get; set; }

    
    }
}