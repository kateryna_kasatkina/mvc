﻿using System.ComponentModel.DataAnnotations;

namespace TTTMVC.Models
{
    public class Player
    {
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public int Points { get; set; }

    }
}