﻿using System.Linq;
using System.Web.Mvc;
using TTTMVC.Models;

namespace KateGame.Controllers
{
    public class HomeController : Controller
    {
        GameContext _db = new GameContext();
        int counter_click = 0;

        public ActionResult Index()
        {
            var model = _db.field.ToList();
            return View(model);
        }

        //public PartialViewResult GetCellValue(int id)
        //{
        //    var val= _db.field
        //        .Where(c => c.ID == id)
        //        .Single();
        //    return PartialView("_GetCellValue", val);
        //}

        //[HttpPost]
        //public ActionResult Create(int restaurantID, Review newReview)
        //{
        //    try
        //    {
        //        var restaurant = _db.Restaurants.Single(r => r.ID == restaurantID);
        //        restaurant.Reviews.Add(newReview);
        //        _db.SaveChanges();

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        public ActionResult Update(int id)
        {
            try
            {
                var cell = _db.field.Single(r => r.ID == id);
                cell.Cell_value = "X";
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}