﻿using System.Web.Mvc;

namespace IdentityMVC5.Helpers
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper html, string alt)
        {
            TagBuilder img = new TagBuilder("img");
            string src = "/Content/Images/code.jpg";
            img.MergeAttribute("src", src);
            img.MergeAttribute("alt", alt);
            img.MergeAttribute("width", "200");
            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }
    }
}