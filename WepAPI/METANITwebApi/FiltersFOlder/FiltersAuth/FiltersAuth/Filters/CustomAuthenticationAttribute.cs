﻿using System;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace Filters.Filters
{
    /// <summary>
    /// Цель фильтров аутентификации - определить, что это за пользователь
    /// При рассмотрении аутентификации в Web API важно понимать ее отличие от ASP.NET MVC.
    /// Web Api не будет перенаправлять пользователя на страницу логина или ошибки, как нередко делается в MVC. Вместо этого клиенту будет отправляться статусный код HTTP 401 (Unauthorized).
    /// </summary>
    public class CustomAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple
        {
            get { return false; }
        }

        //вызывается до обработки запроса для идентификации пользователя
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            //Авторизованный пользователь в ASP.NET представлен объектом интерфейса IPrincipal
            context.Principal = null;

            //получаем заголовки авторизации, которые хранят данные об авторизации: логин и пароль в зашифрованном виде с применением алгоритма Base64.
            AuthenticationHeaderValue authentication = context.Request.Headers.Authorization;
            if(authentication!=null&&authentication.Scheme=="Basic")
            {
                //Чтобы получить логин и пароль, используем декодирование: 
                string[] authData = Encoding.ASCII.GetString(Convert.FromBase64String(authentication.Parameter)).Split(':');
                string[] roles = new string[] { "user" };
                string login = authData[0];
                context.Principal = new GenericPrincipal(new GenericIdentity(login), roles);
            }
            if(context.Principal==null)
            {
                //Если данные об авторизации не установлены и объект IPrincipal равен null, то выдаем ошибку UnauthorizedResult
                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[] { new AuthenticationHeaderValue("Basic") }, context.Request);
            }
            return Task.FromResult<object>(null);
        }

        //вызывается уже когда начинается обработка для отправки ответа клиенту.
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult<object>(null);
        }
    }
}