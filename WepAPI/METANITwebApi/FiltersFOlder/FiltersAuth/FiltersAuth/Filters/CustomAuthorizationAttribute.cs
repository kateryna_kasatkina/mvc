﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Filters.Filters
{
    /// <summary>
    /// цель фильтров авторизации - определение прав аутентифицированного пользователя в системе
    /// </summary>
    public class CustomAuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        private string[] usersList;

        public CustomAuthorizationAttribute(params string[] users)
        {
            this.usersList = users;
        }

        public bool AllowMultiple
        {
            get { return false; }
        }

        //В  ExecuteAuthorizationFilterAsync() мы получаем из контекста объект IPrincipal, который содержит данные о ранее аутентифицированном пользователе.
        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            IPrincipal principal = actionContext.RequestContext.Principal;
            //Если IPrincipal не определен или логин пользователя не входит в число разрешенных, то посылается статусный код Unauthorized
            if (principal==null||!usersList.Contains(principal.Identity.Name))
            {
                return Task.FromResult<HttpResponseMessage>(actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized));
            }
            else
            {
                return continuation();
            }

        }
    }
}