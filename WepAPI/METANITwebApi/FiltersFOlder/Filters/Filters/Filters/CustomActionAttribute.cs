﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Filters.Filters
{
    /// <summary>
    /// Фильтры действий выполняются до и после работы
    /// метода контроллера. С их помощью мы можем изменить
    /// данные запроса, передаваемые в объекте HttpRequestMessage, 
    /// так и внести изменения в результат метода - объект HttpResponseMessage
    /// </summary>
    public class MyCustomActionAttribute : Attribute, IActionFilter
    {
        public bool AllowMultiple
        {
            get { return false; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext">содержит информацию о методе, а также несет ряд данных о запросе</param>
        /// <param name="cancellationToken">представляет токен для отмены операции</param>
        /// <param name="continuation">представляет выполняющийся метод. Вызов этого параметра собственно и предоставляет вызов метода, к которому данный фильтр и будет применяться</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            Stopwatch timer = Stopwatch.StartNew();
            HttpResponseMessage result = await continuation();
            double seconds = timer.ElapsedMilliseconds / 1000.0;
            result.Headers.Add("Elapsed-Time", seconds.ToString());
            return result;
        }
    }
}