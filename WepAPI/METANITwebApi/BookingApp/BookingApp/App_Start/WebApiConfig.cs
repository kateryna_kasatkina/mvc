﻿using BookingApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BookingApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "BookRoute",
                routeTemplate: "api/{controller}/{action}/{id}/{name}/{price}");

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Formatters.Add(new BookFormatter());
        }
    }
}
