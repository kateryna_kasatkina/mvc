﻿using System.Data.Entity;

namespace BookingApp.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext db)
        {
            db.Books.Add(new Book { Name = "Wise man's fear", Year = 2004 });
            db.Books.Add(new Book { Name = "Stone door", Year = 2005 });
            db.Books.Add(new Book { Name = "Lost Girl", Year = 2006 });

            base.Seed(db);
        }
    }
}