﻿using System.Linq;
using System.Web.Http;
using BookingApp.Models;
using System.Web.Http.Description;

namespace BookingApp.Controllers
{
    public class BooksController : ApiController
    {
        private BookContext db = new BookContext();

        public IHttpActionResult Post(Book book)
        {
            if (book == null)
                return BadRequest();
            if(book.Year==1984)
                ModelState.AddModelError("book.Year", "Год не должен быть равен 1984");
            if(book.Name== "Война и мир")
            {
                ModelState.AddModelError("book.Name", "Недопустимое название для книги");
                ModelState.AddModelError("book.Name", "Название не должно начинаться с заглавной буквы");
            }
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            return Ok();
        }

        public int GetValue()
        {
            return 42;
        }

        #region r


        //public Book GetValue([FromUri]Book b)
        //{
        //    return b;
        //}

        // GET: api/Books
        //public IQueryable<Book> GetBooks()
        //{
        //    return db.Books;
        //}

        //// GET: api/Books/5
        //[ResponseType(typeof(Book))]
        //public IHttpActionResult GetBook(int id)
        //{
        //    Book book = db.Books.Find(id);
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(book);
        //}

        //// PUT: api/Books/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutBook(int id, Book book)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != book.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(book).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!BookExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Books
        //[ResponseType(typeof(Book))]
        //public IHttpActionResult PostBook(Book book)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Books.Add(book);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = book.Id }, book);
        //}

        //// DELETE: api/Books/5
        //[ResponseType(typeof(Book))]
        //public IHttpActionResult DeleteBook(int id)
        //{
        //    Book book = db.Books.Find(id);
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Books.Remove(book);
        //    db.SaveChanges();

        //    return Ok(book);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool BookExists(int id)
        //{
        //    return db.Books.Count(e => e.Id == id) > 0;
        //}
        #endregion
    }
}