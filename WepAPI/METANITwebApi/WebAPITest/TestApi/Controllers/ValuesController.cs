﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            string data = "";
            foreach (var o in Request.Headers)
            {
                string val = "";
                foreach (string s in o.Value)
                {
                    val += s + ";";
                }
                data += o.Key + " : " + val;
            }
            return data;
        }

        public string Get(int id)
        {
            // return System.Web.HttpContext.Current.Request.UserHostAddress;
            string requestInfo = "Контроллер: " + ControllerContext.ControllerDescriptor.ControllerName;
            requestInfo += " Url: " + ControllerContext.Request.RequestUri +
                " " + ControllerContext.Request.Method.Method;
            return requestInfo;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
