﻿using ActionAttribute.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace ActionAttribute.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [CustomException(ExceptionType = typeof(IndexOutOfRangeException),
     StatusCode = HttpStatusCode.BadRequest, Message = "Элемент вне диапазона")]
        public string Get(int id)
        {
            string[] letters = new string[] { "aab", "aba", "baa" };
            return letters[id];
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
