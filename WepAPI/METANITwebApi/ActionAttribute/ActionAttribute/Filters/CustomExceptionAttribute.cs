﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace ActionAttribute.Filters
{
    /// <summary>
    /// Фильтры исключений позволяют обработать исключения,
    /// возникшие в методе во время выполнения запроса
    /// </summary>
    public class ArrayExceptionAttribute : Attribute, IExceptionFilter
    {
        public bool AllowMultiple
        {
            get { return true; }
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="actionExecutedContext">В качестве параметров в метод передается контекст HttpActionExecutedContext с полной информацией о запросе</param>
        /// <param name="cancellationToken">токен отмены операции cancellationToken</param>
        /// <returns></returns>
        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if(actionExecutedContext.Exception!=null&&actionExecutedContext.Exception is IndexOutOfRangeException)
            {
                actionExecutedContext.Response= actionExecutedContext.Request.CreateErrorResponse(
                HttpStatusCode.BadRequest, "Элемент вне диапазона");
            }
            return Task.FromResult<object>(null);
        }
    }

    /// <summary>
    /// в данном случае мы не привязаны к конкретному 
    /// типу исключений, к конкретному сообщению или 
    /// статусному коду, которые отправляются в ответ 
    /// пользователю. Все эти данные мы можем задать в 
    /// зависимости от ситуации при использовании атрибута
    /// </summary>
    public class CustomExceptionAttribute : ExceptionFilterAttribute
    {
        public Type ExceptionType { get; set; }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public override Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if ( actionExecutedContext.Exception!=null&&actionExecutedContext.Exception.GetType()==ExceptionType)
            {
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(StatusCode, Message);
            }
            return Task.FromResult<object>(null);
        }
    }
}