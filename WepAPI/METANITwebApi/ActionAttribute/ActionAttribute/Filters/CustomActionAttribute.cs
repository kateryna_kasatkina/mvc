﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WebApiApp.Filters
{
    public class CustomActionAttribute : ActionFilterAttribute
    {
        private DateTime start;

        public override Task OnActionExecutingAsync(HttpActionContext actionContext,
            CancellationToken cancellationToken)
        {
            return Task.Run(() => { start = DateTime.Now; });
        }

        public override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext,
                                        CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                DateTime end = DateTime.Now;
                actionExecutedContext.Response.Headers.Add("Start-Time", start.ToLongTimeString());
                actionExecutedContext.Response.Headers.Add("End-Time", end.ToLongTimeString());
            });
        }
    }
}
