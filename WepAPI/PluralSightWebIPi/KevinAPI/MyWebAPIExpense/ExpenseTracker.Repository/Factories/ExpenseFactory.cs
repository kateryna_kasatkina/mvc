﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using System.Reflection;
using E = ExpenseTracker.Repository.Entities;

namespace ExpenseTracker.Repository.Factories
{
    public class ExpenseFactory
    {
        public ExpenseFactory()
        {

        }

        public MyExpenseTracker.DTO.Expense CreateExpense(E.Expense expense)
        {
            return new MyExpenseTracker.DTO.Expense()
            {
                Amount = expense.Amount,
                Date = expense.Date,
                Description = expense.Description,
                ExpenseGroupId = expense.ExpenseGroupId,
                Id = expense.Id
            };
        }

        public E.Expense CreateExpense(MyExpenseTracker.DTO.Expense expense)
        {
            return new E.Expense()
            {
                Amount = expense.Amount,
                Date = expense.Date,
                Description = expense.Description,
                ExpenseGroupId = expense.ExpenseGroupId,
                Id = expense.Id
            };
        }

        public object CreateDataShapedObject(E.Expense expense, List<string> lstOfFields)
        {

            return CreateDataShapedObject(CreateExpense(expense), lstOfFields);
        }


        public object CreateDataShapedObject(MyExpenseTracker.DTO.Expense expense, List<string> lstOfFields)
        {

            if (!lstOfFields.Any())
            {
                return expense;
            }
            else
            {

                // create a new ExpandoObject & dynamically create the properties for this object

                ExpandoObject objectToReturn = new ExpandoObject();
                foreach (var field in lstOfFields)
                {
                    // need to include public and instance, b/c specifying a binding flag overwrites the
                    // already-existing binding flags.

                    var fieldValue = expense.GetType()
                        .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                        .GetValue(expense, null);

                    // add the field to the ExpandoObject
                    ((IDictionary<String, Object>)objectToReturn).Add(field, fieldValue);
                }

                return objectToReturn;
            }
        }
    }
}
