﻿using System.Linq;

namespace ExpenseTracker.Repository
{
    public interface IExpenseTrackerRepository
    {
        RepositoryActionResult<ExpenseTracker.Repository.Entities.Expense> DeleteExpense(int id);
        RepositoryActionResult<ExpenseTracker.Repository.Entities.ExpenseGroup> DeleteExpenseGroup(int id);
        ExpenseTracker.Repository.Entities.Expense GetExpense(int id, int? expenseGroupId = null);
        ExpenseTracker.Repository.Entities.ExpenseGroup GetExpenseGroup(int id);
        ExpenseTracker.Repository.Entities.ExpenseGroup GetExpenseGroup(int id, string userId);
        IQueryable<Entities.ExpenseGroup> GetExpenseGroups();
        IQueryable<Entities.ExpenseGroup> GetExpenseGroups(string userId);
        ExpenseTracker.Repository.Entities.ExpenseGroupStatus GetExpenseGroupStatus(int id);
        IQueryable<Entities.ExpenseGroupStatus> GetExpenseGroupStatusses();
        IQueryable<Entities.ExpenseGroup> GetExpenseGroupsWithExpenses();
        ExpenseTracker.Repository.Entities.ExpenseGroup GetExpenseGroupWithExpenses(int id);
        ExpenseTracker.Repository.Entities.ExpenseGroup GetExpenseGroupWithExpenses(int id, string userId);
        IQueryable<Entities.Expense> GetExpenses();
        IQueryable<Entities.Expense> GetExpenses(int expenseGroupId);

        RepositoryActionResult<ExpenseTracker.Repository.Entities.Expense> InsertExpense(ExpenseTracker.Repository.Entities.Expense e);
        RepositoryActionResult<ExpenseTracker.Repository.Entities.ExpenseGroup> InsertExpenseGroup(ExpenseTracker.Repository.Entities.ExpenseGroup eg);
        RepositoryActionResult<ExpenseTracker.Repository.Entities.Expense> UpdateExpense(ExpenseTracker.Repository.Entities.Expense e);
        RepositoryActionResult<ExpenseTracker.Repository.Entities.ExpenseGroup> UpdateExpenseGroup(ExpenseTracker.Repository.Entities.ExpenseGroup eg);
    }
}
