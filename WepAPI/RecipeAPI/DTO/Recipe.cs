﻿using System.Collections.Generic;

namespace DTO
{
    public class Recipe
    {
        public int Recipe_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public  List<Ingredient> Ingredients { get; set; }

        public  List<Review> Reviews { get; set; }

        public  List<MyPicture> Picture { get; set; }

        public DishType DishType { get; set; }
    }
}
