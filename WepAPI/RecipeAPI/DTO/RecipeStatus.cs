﻿namespace DTO
{
    public class RecipeStatus
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
