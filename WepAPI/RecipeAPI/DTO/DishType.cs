﻿namespace DTO
{
    public enum DishType
    {
        Hot,
        Cold,
        Salad,
        Sweets,
        Drink
    }
}