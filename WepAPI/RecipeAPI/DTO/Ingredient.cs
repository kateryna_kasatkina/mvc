﻿namespace DTO
{
    public class Ingredient
    {
        public int Ingredient_Id { get; set; }

        public string Name { get; set; }

        public string Quantity { get; set; }

        public string Description { get; set; }

    }
}
