﻿using System;

namespace DTO
{
    public class Review
    {
        public int Review_Id { get; set; }

        public string NameOfReviewer { get; set; }

        public DateTime MyProperty { get; set; }

        public int Rating { get; set; }


    }
}
