﻿using System;
using System.Linq;
using System.Linq.Dynamic;

namespace RecipeAPI.Helpers
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> sourse, string sort)
        {
            if (sourse == null)
            {
                throw new ArgumentNullException("sourse");
            }

            if (sort == null)
            {
                return sourse;
            }

            var lstSort = sort.Split(',');
            string completeSortExpression = "";
            foreach (var sortOption in lstSort)
            {
                // if the sort option starts with "-", we order
                // descending, otherwise ascending
                if (sortOption.StartsWith("-"))
                {
                    completeSortExpression = completeSortExpression + sortOption.Remove(0, 1) + " descending,";
                }
                else
                {
                    completeSortExpression = completeSortExpression + sortOption + ",";
                }
            }

            if (!string.IsNullOrWhiteSpace(completeSortExpression))
            {
                completeSortExpression = completeSortExpression.Remove(completeSortExpression.Count() - 1);
                sourse = sourse.OrderBy(completeSortExpression);
            }

            return sourse;
        }
    }
}