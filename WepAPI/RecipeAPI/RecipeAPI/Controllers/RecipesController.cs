﻿using Repository;
using Repository.Entities;
using Repository.Factories;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using RecipeAPI.Helpers;
using System.Web.Http.Routing;
using System.Web;

namespace RecipeAPI.Controllers
{
    public class RecipesController : ApiController
    {
        IGenericRepository<Recipe> _repository;
        RecipeFactory _recipeFactory = new RecipeFactory();
        const int maxPageSize = 2;

        public RecipesController()
        {
            _repository = new EFGenericRepository<Recipe>(new MyContext());
        }

        //[STAThread]
        [Route("api/recipes", Name = "RecipesList")]
        public IHttpActionResult Get(string sort = "Recipe_Id", int dishType = 0, int page = 1, int pageSize = maxPageSize)
        {
            try
            {
                var recipes = _repository.Get();

                //ensure the page size isn't large than the maximum
                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                //calculate data for metadata
                var totalCount = recipes.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                var urlHelper = new UrlHelper(Request);
                var prevLink = page > 1 ? urlHelper.Link("RecipesList",
                    new
                    {
                        page = page - 1,
                        pageSize = pageSize,
                        sort = sort,
                        dishType = dishType
                    }) : "";
                var nextLink = page < totalPages ? urlHelper.Link("RecipesList",
                    new
                    {
                        page = page + 1,
                        pageSize = pageSize,
                        sort = sort,
                        dishType = dishType
                    }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize = pageSize,
                    totalCount = totalCount,
                    totalPages = totalPages,
                    previousPageLinl = prevLink,
                    nextPageLink = nextLink
                };

                HttpContext.Current.Response.Headers.Add("X-Pagination",
                    Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));

                var result = recipes
                                .ApplySort(sort)
                                .Skip(pageSize * (page - 1))
                                .Take(pageSize)
                                .ToList()
                                .Select(r => _recipeFactory.CreateRecipe(r));

                return Ok(result);
            }
            catch (System.Exception e)
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                var recipe = _repository.FindById(id);

                if (recipe == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(_recipeFactory.CreateRecipe(recipe));
                }
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult Post([FromBody] DTO.Recipe recipe)
        {
            try
            {
                if (recipe == null)
                {
                    return BadRequest();
                }

                var eg = _recipeFactory.CreateRecipe(recipe);
                var result = _repository.Create(eg);
                if (result.Status == RepositoryActionStatus.Created)
                {
                    var newRecipe = _recipeFactory.CreateRecipe
                        (result.Entity);

                    return Created(Request.RequestUri + "/"
                        + newRecipe.Recipe_Id.ToString(), newRecipe);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Put(int id, [FromBody]DTO.Recipe recipe)
        {
            try
            {
                if (recipe == null)
                {
                    return BadRequest();
                }

                var eg = _recipeFactory.CreateRecipe(recipe);
                var result = _repository.Update(id, eg);
                if (result.Status == RepositoryActionStatus.Updated)
                {
                    //map to dto
                    var updatedRecipe = _recipeFactory.CreateRecipe(result.Entity);
                    return Ok(updatedRecipe);
                }
                else if (result.Status == RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }

                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var recipe = _repository.FindById(id);
                var result = _repository.Remove(recipe);

                if (result.Status == RepositoryActionStatus.Deleted)
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else if (result.Status == RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }

                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}
