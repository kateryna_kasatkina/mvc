﻿using Repository;
using E=Repository.Entities;
using Repository.Factories;
using System.Web.Http;
using System.Linq;
using System;
using System.Net;

namespace RecipeAPI.Controllers
{
    [RoutePrefix("api")]
    public class IngredientsController : ApiController
    {
        IGenericRepository<E.Ingredient> _repositoryIngredients;
        IngredientFactory _ingredientFactory = new IngredientFactory();
        IGenericRepository<E.Recipe> _repositoryRecipe;
        public IngredientsController()
        {
            _repositoryIngredients = new EFGenericRepository<E.Ingredient>(new E.MyContext());
            _repositoryRecipe = new EFGenericRepository<E.Recipe>(new E.MyContext());
        }

        //api/recipes/1/ingredients
        [Route("recipes/{recipeId}/ingredients")]
        public IHttpActionResult Get(int recipeId)
        {
            try
            {
                var recipe = _repositoryRecipe.Get(r=>r.Recipe_Id==recipeId).FirstOrDefault();
                if(recipe==null)
                {
                    return NotFound();
                }
                var ingredientsResult = recipe.Ingredients.ToList()
                    .Select(ing => _ingredientFactory.CreateIngredient(ing));
                return Ok(ingredientsResult);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult Get()
        {
            try
            {
                var ingredients = _repositoryIngredients.Get();
                return Ok(ingredients.ToList().Select(ing => _ingredientFactory.CreateIngredient(ing)));
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        //api/recipes/1/ingredients/1
        [Route("recipes/{recipeId}/ingredients/{id}")]
        [Route("ingredients/{id}")]
        public IHttpActionResult Get(int id,int recipeId=0)
        {
            try
            {
                E.Ingredient ingredient = null;
                if(recipeId==0)
                {
                    ingredient =_repositoryIngredients.FindById(id);
                }
                else
                {
                    var ingredientsForRecipe = _repositoryRecipe.FindById(recipeId);
                    //if the recipe doesn't exists, we shouldn't try to get the ingredients
                    if(ingredientsForRecipe!=null)
                    {
                        ingredient = ingredientsForRecipe.Ingredients.FirstOrDefault(ing => ing.Ingredient_Id == id);
                    }
                }

                if(ingredient!=null)
                {
                    var returnValue = _ingredientFactory.CreateIngredient(ingredient);
                    return Ok(returnValue);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("ingredients")]
        public IHttpActionResult Post([FromBody] DTO.Ingredient ingredient)
        {
            try
            {
                if (ingredient == null)
                    return BadRequest();
                var ing = _ingredientFactory.CreateIngredient(ingredient);
                var result = _repositoryIngredients.Create(ing);
                if(result.Status==RepositoryActionStatus.Created)
                {
                    var newIngredient = _ingredientFactory.CreateIngredient(result.Entity);
                    return Created(Request.RequestUri + "/" + newIngredient.Ingredient_Id.ToString(), newIngredient);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }


        [AllowAnonymous]
        [Route("ingredients/{id}")]
        public IHttpActionResult Put(int id,[FromBody]DTO.Ingredient ingredient)
        {
            try
            {
                if(ingredient==null)
                {
                    return BadRequest();
                }

                var ing = _ingredientFactory.CreateIngredient(ingredient);
                var result = _repositoryIngredients.Update(id, ing);
                if(result.Status==RepositoryActionStatus.Updated)
                {
                    var updatedIngredient = _ingredientFactory.CreateIngredient(result.Entity);
                    return Ok(updatedIngredient);
                }
                else if(result.Status==RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

       
        [AllowAnonymous]
        [Route("ingredients/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var ingredient = _repositoryIngredients.FindById(id);
                var result = _repositoryIngredients.Remove(ingredient);
                if(result.Status==RepositoryActionStatus.Deleted)
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else if(result.Status==RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}
