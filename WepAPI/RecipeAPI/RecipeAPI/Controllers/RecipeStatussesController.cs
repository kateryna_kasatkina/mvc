﻿using Repository;
using Repository.Entities;
using Repository.Factories;
using System;
using System.Linq;
using System.Web.Http;
using E = Repository.Entities;

namespace RecipeAPI.Controllers
{
    public class RecipeStatussesController : ApiController
    {
        IGenericRepository<E.RecipeStatus> _repository;
        RecipeMasterDataFactory _recipeMasterDataFactory = new RecipeMasterDataFactory();

        public RecipeStatussesController()
        {
            _repository= new EFGenericRepository<E.RecipeStatus>(new MyContext());
        }

        public RecipeStatussesController(IGenericRepository<E.RecipeStatus> repository)
        {
            _repository = repository;
        }


        public IHttpActionResult Get()
        {
            try
            {
                var recipeStatusses = _repository.Get().ToList()
                    .Select(s => _recipeMasterDataFactory.CreateRecipeStatus(s));
                return Ok(recipeStatusses);

            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}
