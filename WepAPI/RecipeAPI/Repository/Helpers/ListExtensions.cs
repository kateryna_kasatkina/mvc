﻿using System.Collections.Generic;
using System.Linq;

namespace Repository.Helpers
{
    public static class ListExtensions
    {
        public static void RemoveRange<T>(this List<T> sourse,IEnumerable<T> rangeToRemove)
        {
            if (rangeToRemove == null | !rangeToRemove.Any())
                return;
            foreach (T item in rangeToRemove)
            {
                sourse.Remove(item);
            }
        }
    }
}
