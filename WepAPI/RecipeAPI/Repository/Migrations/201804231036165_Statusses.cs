namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Statusses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RecipeStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Recipes", "RecipeStatus_Id", c => c.Int());
            CreateIndex("dbo.Recipes", "RecipeStatus_Id");
            AddForeignKey("dbo.Recipes", "RecipeStatus_Id", "dbo.RecipeStatus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Recipes", "RecipeStatus_Id", "dbo.RecipeStatus");
            DropIndex("dbo.Recipes", new[] { "RecipeStatus_Id" });
            DropColumn("dbo.Recipes", "RecipeStatus_Id");
            DropTable("dbo.RecipeStatus");
        }
    }
}
