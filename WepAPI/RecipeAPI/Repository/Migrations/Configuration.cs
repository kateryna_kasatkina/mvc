namespace Repository.Migrations
{
    using Repository.Entities;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.Entities.MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MyContext context)
        {
            context.Recipes.AddOrUpdate(new Recipe
            {
                Name = "���� � ������ � ����",
                Description = "��������� ���� � ������� �������, ������� ������, �������� ������ ���, �������� �����",
                Ingredients = new List<Ingredient>
                {
                    new Ingredient{Name="����", Quantity="2 ��"},
                    new Ingredient{Name="�����", Quantity="15 ��"},
                    new Ingredient{Name="���", Quantity="5 ��"},
                    new Ingredient{Name="����", Quantity="400 ��"},
                    new Ingredient{Name="�����", Quantity="20 ��"},
                },
                Picture = new List<MyPicture> { new MyPicture { MyUrl = "./Content/Images/2.jpg" },
                },
                DishType = Repository.DishType.Cold

            });

            context.Recipes.AddOrUpdate(new Recipe
            {
                Name = "��������� � ��������",
                Description = "�������� ����, �������� �������, ���� ������� ������, ������ ��������� ���������� �������",
                Ingredients = new List<Ingredient>
                {
                    new Ingredient{Name="����", Quantity="1 ��"},
                    new Ingredient{Name="�����", Quantity="15 ��"},
                    new Ingredient{Name="�������", Quantity="50 ��"},
                },
                Picture = new List<MyPicture> { new MyPicture { MyUrl = "./Content/Images/3.jpg" } },
                DishType=Repository.DishType.Hot
            });
        }
    }
}
