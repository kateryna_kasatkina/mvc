namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DishType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Recipes", "DishType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Recipes", "DishType");
        }
    }
}
