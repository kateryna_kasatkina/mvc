namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Ingredient_Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Quantity = c.String(nullable: false),
                        Description = c.String(),
                        Recipe_Recipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Ingredient_Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Recipe_Id)
                .Index(t => t.Recipe_Recipe_Id);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Recipe_Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Recipe_Id);
            
            CreateTable(
                "dbo.MyPictures",
                c => new
                    {
                        MyPicture_Id = c.Int(nullable: false, identity: true),
                        MyUrl = c.String(),
                        Recipe_Recipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.MyPicture_Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Recipe_Id)
                .Index(t => t.Recipe_Recipe_Id);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Review_Id = c.Int(nullable: false, identity: true),
                        NameOfReviewer = c.String(maxLength: 100),
                        MyProperty = c.DateTime(nullable: false),
                        Rating = c.Int(nullable: false),
                        Recipe_Recipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Review_Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Recipe_Id)
                .Index(t => t.Recipe_Recipe_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "Recipe_Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.MyPictures", "Recipe_Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.Ingredients", "Recipe_Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.Reviews", new[] { "Recipe_Recipe_Id" });
            DropIndex("dbo.MyPictures", new[] { "Recipe_Recipe_Id" });
            DropIndex("dbo.Ingredients", new[] { "Recipe_Recipe_Id" });
            DropTable("dbo.Reviews");
            DropTable("dbo.MyPictures");
            DropTable("dbo.Recipes");
            DropTable("dbo.Ingredients");
        }
    }
}
