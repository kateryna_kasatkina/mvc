﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.Entities
{
    public class Recipe
    {
        [Key]
        public int Recipe_Id { get; set; }

        [Required]

        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual List<Ingredient> Ingredients { get; set; }

        public virtual List<Review> Reviews { get; set; }

        public virtual List<MyPicture> Picture { get; set; }

        [Required]
        public DishType DishType { get; set; }
    }
}
