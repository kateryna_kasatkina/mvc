﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Entities
{
    public class Review
    {
        [Key]
        public int Review_Id { get; set; }

        [StringLength(100)]
        public string NameOfReviewer { get; set; }

        public DateTime MyProperty { get; set; }

        public int Rating { get; set; }


    }
}
