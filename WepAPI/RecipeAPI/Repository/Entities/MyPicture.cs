﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Entities
{
    public class MyPicture
    {
        [Key]
        public int MyPicture_Id { get; set; }

        public string MyUrl { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
