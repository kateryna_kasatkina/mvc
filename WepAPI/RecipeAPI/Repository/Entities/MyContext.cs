﻿using System.Data.Entity;

namespace Repository.Entities
{
    public class MyContext : DbContext
    {
        public MyContext() : base("RecipebookAPI") { }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<MyPicture> Pictures { get; set; }
        public DbSet<RecipeStatus> RecipeStatusses { get; set; }

    }
}
