﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Entities
{
    public class Ingredient
    {
        [Key]
        public int Ingredient_Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        public string Quantity { get; set; }

        public string Description { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
