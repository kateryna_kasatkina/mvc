﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Get();

        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);

        TEntity FindById(int id);

        RepositoryActionResult<TEntity> Create(TEntity item);

        RepositoryActionResult<TEntity> Update(int id, TEntity item);

        RepositoryActionResult<TEntity> Remove(TEntity item);


    }
}