﻿using System;
using System.Linq;
using Repository.Entities;
using System.Data.Entity;
using System.Collections.Generic;

namespace Repository
{
    public class EFGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        MyContext _context;
        DbSet<TEntity> _dbSet;

        public EFGenericRepository(MyContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get()
        {
            return _dbSet.AsNoTracking();
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }
        public TEntity FindById(int id)
        {
            return _dbSet.Find(id);
        }

        public RepositoryActionResult<TEntity> Create(TEntity item)
        {
             try
            {
                 _dbSet.Add(item);
                var result = _context.SaveChanges();
                if (result > 0)
                {
                    return new RepositoryActionResult<TEntity>(item, RepositoryActionStatus.Created);
                }
                else
                {
                    return new RepositoryActionResult<TEntity>(item, RepositoryActionStatus.NothingModified, null);
                }
            }
            catch (Exception ex)
            {
                return new RepositoryActionResult<TEntity>(null, RepositoryActionStatus.Error, ex);
            }

        }

        public RepositoryActionResult<TEntity> Update(int id,TEntity item)
        {
            //_context.Entry(item).State = EntityState.Modified;
            //_context.SaveChanges();

            try
            {
                var existingRecipe = _context.Recipes.FirstOrDefault(r => r.Recipe_Id == id);
                if(existingRecipe==null)
                {
                    return new RepositoryActionResult<TEntity>(item, RepositoryActionStatus.NotFound);
                }

                _context.Entry(item).State = EntityState.Modified;
                var result=_context.SaveChanges();
                if(result>0)
                {
                    return new RepositoryActionResult<TEntity>(item, RepositoryActionStatus.Updated);
                }
                else
                {
                    return new RepositoryActionResult<TEntity>(item, RepositoryActionStatus.NothingModified, null);
                }
            }
            catch (Exception ex)
            {
                return new RepositoryActionResult<TEntity>(null, RepositoryActionStatus.Error,ex);
            }
        }

        public RepositoryActionResult<TEntity> Remove(TEntity item)
        {
            //_dbSet.Remove(item);
            //_context.SaveChanges();

            try
            {
                
                if(item!=null)
                {
                    _dbSet.Remove(item);
                    _context.SaveChanges();
                    return new RepositoryActionResult<TEntity>(null, RepositoryActionStatus.Deleted);
                }
                return new RepositoryActionResult<TEntity>(null, RepositoryActionStatus.NotFound);
            }
            catch (Exception ex)
            {
                return new RepositoryActionResult<TEntity>(null, RepositoryActionStatus.Error, ex);
            }
        }

       
    }
}
