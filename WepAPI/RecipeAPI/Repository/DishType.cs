﻿namespace Repository
{
    public enum DishType
    {
        Hot,
        Cold,
        Salad,
        Sweets,
        Drink
    }
}
