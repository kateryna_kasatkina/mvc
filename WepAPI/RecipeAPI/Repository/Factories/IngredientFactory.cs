﻿using E = Repository.Entities;

namespace Repository.Factories
{
    public class IngredientFactory
    {
        public IngredientFactory()
        {

        }

        public DTO.Ingredient CreateIngredient(E.Ingredient ingredient)
        {
            return new DTO.Ingredient()
            {
                Name=ingredient.Name,
                Description=ingredient.Description,
                Quantity=ingredient.Quantity,
                Ingredient_Id=ingredient.Ingredient_Id
            };
        }

        public E.Ingredient CreateIngredient(DTO.Ingredient ingredient)
        {
            return new E.Ingredient()
            {
                Name = ingredient.Name,
                Description = ingredient.Description,
                Quantity = ingredient.Quantity,
                Ingredient_Id = ingredient.Ingredient_Id
            };
        }
    }
}
