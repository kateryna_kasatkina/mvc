﻿using Repository.Entities;

namespace Repository.Factories
{
    public class RecipeMasterDataFactory
    {
        public RecipeMasterDataFactory()
        {

        }

        public RecipeStatus CreateRecipeStatus(DTO.RecipeStatus recipeStatus)
        {
            return new RecipeStatus()
            {
                Description = recipeStatus.Description,
                Id = recipeStatus.Id
            };
        }

        public DTO.RecipeStatus CreateRecipeStatus(RecipeStatus recipeStatus)
        {
            return new DTO.RecipeStatus()
            {
                Description = recipeStatus.Description,
                Id = recipeStatus.Id
            };
        }
    }
}
