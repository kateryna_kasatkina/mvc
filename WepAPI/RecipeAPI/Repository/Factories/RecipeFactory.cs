﻿using System.Collections.Generic;
using System.Linq;
using E = Repository.Entities;

namespace Repository.Factories
{
    public class RecipeFactory
    {
        IngredientFactory IngredientFactory = new IngredientFactory();

        public RecipeFactory()
        {

        }

        public E.Recipe CreateRecipe(DTO.Recipe recipe)
        {
            return new E.Recipe
            {
                Recipe_Id=recipe.Recipe_Id,
                Name=recipe.Name,
                Description=recipe.Description,
                Ingredients=recipe.Ingredients==null?new List<E.Ingredient>():recipe.Ingredients.Select(r=>IngredientFactory.CreateIngredient(r)).ToList()
            };
        }

        public DTO.Recipe CreateRecipe(E.Recipe recipe)
        {
            return new DTO.Recipe
            {
                Recipe_Id = recipe.Recipe_Id,
                Name = recipe.Name,
                Description = recipe.Description,
                Ingredients = recipe.Ingredients.Select(r => IngredientFactory.CreateIngredient(r)).ToList()
            };
        }
    }
}
