﻿using KateOdeToFood.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace KateOdeToFood.Controllers
{
    public class ReviewsController : Controller
    {
        OdeToFoodDB _db = new OdeToFoodDB();
        // GET: Reviews
        public ActionResult Index()
        {
            var model = _db.Reviews.Include(r => r.Restaurant).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var review = _db.Reviews
                .Where(c => c.ID == id)
                .FirstOrDefault();
            return View(review);
        }
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
                var review = _db.Reviews
                .Where(c => c.ID == id)
                .FirstOrDefault();
                TryUpdateModel(review);

            if(TryUpdateModel(review))
            {
                return RedirectToAction("index");
            }
                return View(review);
        }

        public ActionResult BestReview()
        {
            var model= _db.Reviews
                .FirstOrDefault();
            return PartialView("_Review", model);
        }

        public ActionResult Create()
        {
            return View(new Review());
        }

        //
        // POST: /Reviews/Create

        [HttpPost]
        public ActionResult Create(int restaurantID, Review newReview)
        {
            try
            {
                var restaurant = _db.Restaurants.Single(r => r.ID == restaurantID);
                restaurant.Reviews.Add(newReview);
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}