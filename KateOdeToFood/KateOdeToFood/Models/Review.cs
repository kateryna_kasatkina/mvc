﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KateOdeToFood.Models
{
    public class Review:IValidatableObject
    {
        public int ID { get; set; }
        public virtual Restaurant Restaurant { get; set; }

        [Range(1,10)]
        public int Rating { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }

        [DisplayName("Dining date")]
        //[DisplayFormat(DataFormatString ="{0:d}",ApplyFormatInEditMode =true)]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

        public IEnumerable<ValidationResult>
            Validate(ValidationContext validationContext)
        {
            var field = new[] { "Created" };
            if (Created > DateTime.Now)
            {
                yield return new ValidationResult("Create date can not be in the future",field);
            }
            if(Created <DateTime.Now.AddYears(-1))
            {
                yield return new ValidationResult("Create date cannot be too far in the past",field);
            }
        }
    }
}