﻿using KateOdeToFood.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace KateOdeToFood.Controllers
{
    public class OdeToFoodDB:DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Restaurant>()
                .Property(r => r.ID).HasColumnName("restaurant_id");
            modelBuilder.Entity<Restaurant>()
                .HasMany(restaurant => restaurant.Reviews)
                .WithRequired(review => review.Restaurant);
            modelBuilder.Entity<Review>()
                .ToTable("tbl_Reviews");
            base.OnModelCreating(modelBuilder);
        }



    }
}