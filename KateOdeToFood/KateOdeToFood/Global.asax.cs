﻿using KateOdeToFood.Controllers;
using KateOdeToFood.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace KateOdeToFood
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new OdeToFoodDBInitializer());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public class OdeToFoodDBInitializer : DropCreateDatabaseIfModelChanges<OdeToFoodDB>
        {
            protected override void Seed(OdeToFoodDB context)
            {
                base.Seed(context);
                context.Restaurants.Add(new Restaurant
                {
                    Name = "Marrakesh",
                    Address = new Address
                    {
                        City = "Washington",
                        State = "D.C.",
                        Country = "USA"
                    },
                   
                  
                });

                context.Restaurants.Add(new Restaurant
                {
                    Name = "Sabatino's",
                    Address = new Address
                    {
                        City = "Baltimore",
                        State = "MD",
                        Country = "USA"
                    }
                });

                context.Restaurants.Add(new Restaurant
                {
                    Name = "The Kings Contrivance",
                    Address = new Address
                    {
                        City = "Columbia",
                        State = "MD",
                        Country = "USA"
                    }
                });


                context.Reviews.Add(new Review
                {
                    Body = "A fantistic culinary pleasure.",
                    Created = DateTime.Now,
                    ID = 1,
                    Rating = 9,
                    Restaurant = new Restaurant
                    {
                        Name = "Marrakesh",
                        Address = new Address
                        {
                            City = "Washington",
                            State = "D.C.",
                            Country = "USA"
                        }
                    }
                });

                context.Reviews.Add(new Review
                {
                    Body = "Only eat here if you must.",
                    Created = DateTime.Now,
                    ID = 2,
                    Rating = 3,
                    Restaurant = new Restaurant
                    {
                        Name = "School Cafeteria",
                        Address = new Address
                        {
                            City = "Baltimore",
                            State = "MD",
                            Country = "USA"
                        }
                    }
                });

                context.Reviews.Add(new Review
                {
                    Body = "I'm lucky to still be alive.",
                    Created = DateTime.Now,
                    ID = 3,
                    Rating = 3,
                    Restaurant = new Restaurant
                    {
                        Name = "Joe's Deep Fried Bacon Cheesburger Shack",
                        Address = new Address
                        {
                            City = "Columbia",
                            State = "MD",
                            Country = "USA"
                        }
                    }
                });

                context.SaveChanges();
            }
        }
    }
}

