﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.WebUI.Models
{
    public class PagingInfo
    {
        //number of goods
        public int TotalItems { get; set; }

        //number of goods displayed on one page
        public int ItemsPerPage { get; set; }

        //number of current page
        public int CurrentPage { get; set; }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
            }
        }

    }
}