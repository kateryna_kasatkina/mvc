﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Domain.Enteties
{
    public class Cart
    {
        private List<Cartline> lineCollection = new List<Cartline>();
        public void AddItem(Game game,int quantity)
        {
            Cartline line = lineCollection
                .Where(g => g.Game.GameId == game.GameId)
                .FirstOrDefault();

            if (line==null)
            {
                lineCollection.Add(new Cartline
                { Game = game, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Game game)
        {
            lineCollection.RemoveAll(l => l.Game.GameId == game.GameId);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Game.Price * e.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        /// <summary>
        ///  свойство, которое позволяет обратиться к 
        ///  содержимому корзины с использованием IEnumerable<CartLine>
        /// </summary>
        public IEnumerable<Cartline> Lines
        {
            get { return lineCollection; }
        }
    }

    /// <summary>
    /// представляет товар, выбранный пользователем,
    /// а также приобретаемое его количество
    /// </summary>
    public class Cartline
    {
        public Game Game { get; set; }
        public int Quantity { get; set; }
    }

}
