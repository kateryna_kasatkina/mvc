﻿using GameStore.Domain.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Domain.Abstract
{
    public  interface IOrderProcesso
    {
        void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
    }
}
