﻿using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class ulController : ApiController
    {
        //generate labyrinth from users .dll using reflection
        public IHttpActionResult Get()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/");
            
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, "*.dll");
                if (files.Length > 1)
                {
                    return InternalServerError();
                }
                string file = files[0];

                //generate response base on user defined library 
                string aPath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/MazeDLL.dll");
                var DLL = Assembly.LoadFile(aPath);

                Type[] typesInThisAssembly = DLL.GetTypes();

                Type listTransferType = DLL.GetType("MazeDLL.ListTransfer");
                object listTransfer = Activator.CreateInstance(listTransferType);

                foreach (Type type in typesInThisAssembly)
                {
                    if (type.GetInterface("ILabyrinth") != null)
                    {
                        //Create instance from dynamically loaded assembly
                        Type labyrinthType = DLL.GetType(type.ToString());
                        object labyrinthInstance = Activator.CreateInstance(labyrinthType);
                        listTransfer = labyrinthType.InvokeMember("CreateLabyrinth",
                            BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                            null, labyrinthInstance, null);

                        break;
                    }
                }

                //delete file from directory
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                //foreach (FileInfo f in directoryInfo.EnumerateFiles())
                //{
                //    f.Delete();
                //}

                return Ok(listTransfer);
            }
            return InternalServerError();
        }

        //save users .dll 
        public IHttpActionResult Post(HttpPostedFileBase postedFile)
        {
            if (postedFile != null)
            {
                string path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/");
                string fileName = Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(path + "MazeDLL.dll");
                return Ok();
            }
            return InternalServerError();
        }
    }
}
