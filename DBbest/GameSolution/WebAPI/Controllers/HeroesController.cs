﻿using DataLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HeroesController : ApiController
    {
        GameContext _db = new GameContext();

        public IHttpActionResult Get()
        {
            List<Hero> heroes = _db.Heroes.Take(10).ToList();

            return Ok(heroes);
        }

        public IHttpActionResult Post([FromBody] Hero hero)
        {
            if (hero == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _db.Heroes.Add(hero);
            _db.SaveChanges();

            return Ok();
        }
    }
}
