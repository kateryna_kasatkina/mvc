﻿using PacmanDLL;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LabyrinthController : ApiController
    {
        public IHttpActionResult Get()
        {
            //default library for creating maze LabyrinthDLL
            Labyrinth myMaze = new Labyrinth();
            List<Cell>list=myMaze.GenerateCells();
            List<List<Cell>> result = myMaze.CreateLabyrinth(list);
            return Ok(result);
        }
    }
}


