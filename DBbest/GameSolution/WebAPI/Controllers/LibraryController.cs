﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;


namespace WebAPI.Controllers
{
    public class LibraryController : ApiController
    {
        public IHttpActionResult Get()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/MazeDLL.dll");

            var dataBytes = File.ReadAllBytes(path);

            var stream = new MemoryStream(dataBytes,0, dataBytes.Length,true,true);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.GetBuffer())
            };

            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "MazeDLL.dll"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            var response = ResponseMessage(result);

            return response;
        }

        public IHttpActionResult Post(HttpPostedFileBase postedFile)
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/");
            if (postedFile != null)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(path + fileName);
            }


            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, "*.dll");
                if (files.Length > 1)
                {
                    return InternalServerError();
                }
                string file = files[0];

                //generate response base on user defined library 
                var DLL = Assembly.LoadFile("~/Uploads" + file);
                Type[] typesInThisAssembly = DLL.GetTypes();

                Type listTransferType = DLL.GetType("ListTransfer");
                object listTransfer = Activator.CreateInstance(listTransferType);

                foreach (Type type in typesInThisAssembly)
                {
                    if (type.GetInterface("ILabyrinth") != null)
                    {
                        //Create instance from dynamically loaded assembly
                        Type labyrinthType = DLL.GetType(type.ToString());
                        object labyrinthInstance = Activator.CreateInstance(labyrinthType);
                        listTransfer = labyrinthType.InvokeMember("CreateLabyrinth",
                            BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                            null, labyrinthInstance, null);

                        break;
                    }
                }

                //delete file from directory
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                foreach (FileInfo f in directoryInfo.EnumerateFiles())
                {
                    f.Delete();
                }

                return Ok(listTransfer);
            }
            return InternalServerError();
        }
    }
}
