﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities
{
    public class Hero
    {
        [Key]
        public int Heroe_Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public int Points { get; set; }
    }
}
