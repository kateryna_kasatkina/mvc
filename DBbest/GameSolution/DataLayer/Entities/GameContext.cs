﻿using System.Data.Entity;

namespace DataLayer.Entities
{
    public class GameContext : DbContext
    {
        public GameContext() : base("GameAPI") { }

        public DbSet<Hero> Heroes { get; set; }
    }
}
