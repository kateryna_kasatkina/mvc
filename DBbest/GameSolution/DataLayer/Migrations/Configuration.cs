namespace DataLayer.Migrations
{
    using DataLayer.Entities;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DataLayer.Entities.GameContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataLayer.Entities.GameContext context)
        {
            context.Heroes.AddOrUpdate(new Hero { Name = "Hero2", Points = 20 },
                new Hero { Name = "Hero3", Points = 30 },
                new Hero { Name = "Hero4", Points = 40 },
                new Hero { Name = "Hero5", Points = 50 });
        }
    }
}
