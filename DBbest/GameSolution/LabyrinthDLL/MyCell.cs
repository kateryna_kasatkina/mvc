﻿
using System.Collections.Generic;
using System.Linq;

namespace LabyrinthDLL
{
    /// <summary>
    /// This class represent a single cell in grid(labirynth)
    /// </summary>
    public class MyCell
    {
        // Position in the maze
        public int Row { get; }
        public int Column { get; }

        // Neighboring cells

        public MyCell North { get; set; }
        public MyCell South { get; set; }
        public MyCell East { get; set; }
        public MyCell West { get; set; }

        public List<MyCell> Neighbors
        {
            get { return new[] { North, South, East, West }.Where(c => c != null).ToList(); }
        }

        // Cells that are linked to this cell
        private readonly Dictionary<MyCell, bool> _links;
        public List<MyCell> Links => _links.Keys.ToList();

        public MyCell(int row, int col)
        {
            Row = row;
            Column = col;
            _links = new Dictionary<MyCell, bool>();
        }

        public void Link(MyCell cell, bool bidirectional = true)
        {
            _links[cell] = true;
            if (bidirectional)
            {
                cell.Link(this, false);
            }
        }

        public void Unlink(MyCell cell, bool bidirectional = true)
        {
            _links.Remove(cell);
            if (bidirectional)
            {
                cell.Unlink(this, false);
            }
        }

        public bool IsLinked(MyCell cell)
        {
            if (cell == null)
            {
                return false;
            }
            return _links.ContainsKey(cell);
        }
    }
}
