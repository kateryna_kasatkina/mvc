﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace LabyrinthDLL
{
    public class Labyrinth : ILabyrinth
    {
        public ListTransfer CreateLabyrinth()
        {
            int seed = -1;
            Matrix grid = new Matrix(10, 10);
            var rand = seed >= 0 ? new Random(seed) : new Random();
            foreach (var cell in grid.Cells)
            {
                var neighbors = new[] { cell.North, cell.East }.Where(c => c != null).ToList();
                if (!neighbors.Any())
                {
                    continue;
                }
               // var neighbor = neighbors.Sample(rand);
                var neighbor=neighbors[rand.Next(neighbors.Count)];
                if (neighbor != null)
                {
                    cell.Link(neighbor);
                }
            }
            ListTransfer result = PrepareForTransfer(grid);
            //Console.WriteLine(grid.ToString()); 
            return result;
        }

        public ListTransfer PrepareForTransfer(Matrix grid)
        {
            List<Cell> list = new List<Cell>();
            for (int row = 0; row < grid.Rows; row++)
            {
                for (int col = 0; col < grid.Columns; col++)
                {
                    Cell transfer = new Cell
                    {
                        //North, South, East, West
                        North = grid[row, col].IsLinked(grid[row, col].North),
                        South = grid[row, col].IsLinked(grid[row, col].South),
                        East = grid[row, col].IsLinked(grid[row, col].East),
                        West = grid[row, col].IsLinked(grid[row, col].West)
                    };
                    list.Add(transfer);
                }
            }
            ListTransfer tlist = new ListTransfer
            {
                List = list,
                Rows = grid.Rows,
                Cols = grid.Columns
            };

            return tlist;
        }
    }
}
