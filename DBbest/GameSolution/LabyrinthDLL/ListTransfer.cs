﻿using System.Collections.Generic;

namespace LabyrinthDLL
{
    public class ListTransfer
    {
        public int Rows { get; set; }
        public int Cols { get; set; }
        public List<Cell> List { get; set; }
    }
}
