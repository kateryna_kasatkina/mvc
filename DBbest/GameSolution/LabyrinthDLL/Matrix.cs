﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabyrinthDLL
{
    public class Matrix
    {
        public int Rows { get; }
        public int Columns { get; }
        public int Size => Rows * Columns;

        public List<List<MyCell>> Grid { get; set; }

        public virtual MyCell this[int row, int column]
        {
            get
            {
                if (row < 0 || row >= Rows)
                {
                    return null;
                }
                if (column < 0 || column >= Columns)
                {
                    return null;
                }
                return Grid[row][column];
            }
        }

        public MyCell RandomCell()
        {
            var rand = new Random();
            var row = rand.Next(Rows);
            var col = rand.Next(Columns);
            var randomCell = this[row, col];
            if (randomCell == null)
            {
                throw new InvalidOperationException("Random cell is null");
            }
            return randomCell;
        }

        // Row iterator
        public IEnumerable<List<MyCell>> Row
        {
            get
            {
                foreach (var row in Grid)
                {
                    yield return row;
                }
            }
        }

        // Cell iterator
        public IEnumerable<MyCell> Cells
        {
            get
            {
                foreach (var row in Row)
                {
                    foreach (var cell in row)
                    {
                        yield return cell;
                    }
                }
            }
        }

        public Matrix(int rows, int cols)
        {
            Rows = rows;
            Columns = cols;

            PrepareGrid();
            ConfigureCells();
        }

        private void PrepareGrid()
        {
            Grid = new List<List<MyCell>>();
            for (var r = 0; r < Rows; r++)
            {
                var row = new List<MyCell>();
                for (var c = 0; c < Columns; c++)
                {
                    row.Add(new MyCell(r, c));
                }
                Grid.Add(row);
            }
        }

        private void ConfigureCells()
        {
            foreach (var cell in Cells)
            {
                var row = cell.Row;
                var col = cell.Column;

                cell.North = this[row - 1, col];
                cell.South = this[row + 1, col];
                cell.West = this[row, col - 1];
                cell.East = this[row, col + 1];
            }
        }

        public override string ToString()
        {
            var output = new StringBuilder("+");
            for (var i = 0; i < Columns; i++)
            {
                output.Append("---+");
            }
            output.AppendLine();

            foreach (var row in Row)
            {
                var top = "|";
                var bottom = "+";
                foreach (var cell in row)
                {
                    const string body = "   ";
                    var east = cell.IsLinked(cell.East) ? " " : "|";

                    top += body + east;

                    var south = cell.IsLinked(cell.South) ? "   " : "---";
                    const string corner = "+";
                    bottom += south + corner;
                }
                output.AppendLine(top);
                output.AppendLine(bottom);
            }

            return output.ToString();
        }
    }
}
