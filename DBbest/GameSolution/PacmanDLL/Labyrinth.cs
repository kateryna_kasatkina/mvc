﻿using System;
using System.Collections.Generic;

namespace PacmanDLL
{
    public class Labyrinth : ILabyrinyh
    {
        public List<Cell> GenerateCells()
        {
            List<Cell> allCells = new List<Cell>
            {
                //pacman
                new Cell() { IsWall = false, IsPacman = true, IsEnemy = false, IsEmpty = false,IsVisited=true }
            };
            //enemies
            for (int i = 0; i < 6; i++)
            {
                allCells.Add(new Cell() { IsWall = false, IsPacman = false, IsEnemy = true, IsEmpty = false, IsVisited = false });
            }
            //wals
            for (int i = 0; i < 30; i++)
            {
                allCells.Add(new Cell() { IsWall = true, IsPacman = false, IsEnemy = false, IsEmpty = false, IsVisited = true });
            }
            //apples
            for (int i = 0; i < 63; i++)
            {
                allCells.Add(new Cell() { IsWall = false, IsPacman = false, IsEnemy = false, IsEmpty = false, IsVisited = false });
            }


            Random random = new Random();
            for (int i = allCells.Count-1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                var temp = allCells[j];
                allCells[j] = allCells[i];
                allCells[i] = temp;
            }

            return allCells;

        }

        public List<List<Cell>> CreateLabyrinth(List<Cell> mas)
        {
            List<List<Cell>> list = new List<List<Cell>>();

            int counter = 0;
            for (int j = 0; j < 10; j++)
            {
                List<Cell> l = new List<Cell>();
                for (int i = 0; i <10 &&counter<100; i++)
                {
                    if(j==0||j==9||i==0||i==9)
                    {
                        mas[counter].IsWall = false;
                        mas[counter].IsEmpty = false;
                    }
                    
                    mas[counter].Y = j;
                    mas[counter].X = i;
                    l.Add(mas[counter]);
                    counter++;
                }
                list.Add(l);
            }
            bool IsOk = IsGoodField(list);
            if (IsOk == true)
                return list;

            return list;

        }


        public bool IsGoodField(List<List<Cell>> list)
        {
            List<Cell> visited = new List<Cell>();
            //find pacman
            Cell pacman = new Cell();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (list[i][j].IsPacman == true)
                    {
                        pacman = list[i][j];
                    }
                    if (list[i][j].IsVisited == true)
                    {
                        visited.Add(list[i][j]);
                    }
                }
            }

            //use principle of right hand
            //bool isWay=true;
            // && isWay == true
            int number = 100 - 31;
            int coordXincrement = 0;
            int coordXdecrement = 0;
            int coordYincrement = 0;
            int coordYdecrement = 0;
            for (int i = 0; i < number; i++)
            {
                coordXincrement = (pacman.X) + 1;
                coordXdecrement= (pacman.X) - 1;
                coordYincrement = (pacman.Y) + 1;
                coordYdecrement = (pacman.Y) - 1;
                //go right
                if (pacman.X < 9 && pacman.X > 0 && list[pacman.Y][++pacman.X].IsWall == false)
                {
                    list[pacman.Y][coordXincrement].IsVisited = true;
                    pacman = list[pacman.Y][coordXincrement];
                    visited.Add(list[pacman.Y][coordXincrement]);
                    continue;
                }
                //if on right hand is wall go down
                if (pacman.Y < 9 && pacman.Y > 0 && list[coordYincrement][pacman.X].IsWall == false)
                {
                    list[coordYincrement][pacman.X].IsVisited = true;
                    pacman = list[coordYincrement][pacman.X];
                    visited.Add(list[coordYincrement][pacman.X]);
                    continue;
                }
                //if no right and no down - go left
                if (pacman.X > 0 && pacman.X < 10 && list[pacman.Y][coordXdecrement].IsWall == false )
                {
                    list[pacman.Y][coordXdecrement].IsVisited = true;
                    pacman = list[pacman.Y][coordXdecrement];
                    visited.Add(list[pacman.Y][coordXdecrement]);
                    continue;
                }
                //if no right, no down, no left, go up
                if (pacman.Y > 0 && pacman.Y < 10 && list[coordYdecrement][pacman.X].IsWall == false )
                {
                    list[coordYdecrement][pacman.X].IsVisited = true;
                    pacman = list[coordYdecrement][pacman.X];
                    visited.Add(list[coordYdecrement][pacman.X]);
                    continue;
                }

            }

           
            var counterVisited = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                   if( list[i][j].IsVisited == true)
                    counterVisited++;
                }
            }
            Console.WriteLine(counterVisited);
          
            if ((100 - counterVisited) != 0)
                return false;
            else
                return true;

        }



        #region

        //public List<List<Cell>> CreateUnit()
        //{
        //    List<List<Cell>> Unitlist = new List<List<Cell>>();


        //    //create a list with empte cells first line
        //    List<Cell> firstLine = new List<Cell>()
        //    {
        //        new Cell() {IsFree=false, IsWall = false, IsPacman = true, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false,IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false,IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false,IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},

        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false},
        //    };
        //    Unitlist.Add(firstLine);

        //    List<Cell> secondLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=false,IsPacman=false,IsEnemy=true ,IsEmpty=false},

        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(secondLine);

        //    List<Cell> thirdLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},

        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(thirdLine);

        //    List<Cell> fouthLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},

        //         new Cell(){IsFree=false, IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=false, IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=false, IsWall=false,IsPacman=false,IsEnemy=true ,IsEmpty=false},
        //         new Cell(){IsFree=false, IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=true, IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(fouthLine);

        //    List<Cell> fifthhLine = new List<Cell>()
        //    {
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },

        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false,IsEmpty=false },
        //    };
        //    Unitlist.Add(fifthhLine);

        //    List<Cell> sixLine = new List<Cell>()
        //    {
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},

        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true, IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=false, IsWall = false, IsPacman = false, IsEnemy = true ,IsEmpty=false},
        //    };            
        //    Unitlist.Add(sixLine);

        //    List<Cell> sevenLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},

        //         new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //         new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(sevenLine);

        //    List<Cell> eightLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},

        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(eightLine);

        //    List<Cell> nineLine = new List<Cell>() {
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},

        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=false,IsWall=true,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //        new Cell(){IsFree=true,IsWall=false,IsPacman=false,IsEnemy=false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(nineLine);

        //    List<Cell> tenLine = new List<Cell>()
        //    {
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},

        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //        new Cell() {IsFree=true,IsWall = false, IsPacman = false, IsEnemy = false ,IsEmpty=false},
        //    };
        //    Unitlist.Add(tenLine);

        //    return Unitlist;
        //}
        #endregion

    }
}
