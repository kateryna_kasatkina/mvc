﻿namespace PacmanDLL
{
    public class Cell
    {
        public bool IsWall { get; set; }
        public bool IsPacman { get; set; }
        public bool IsEnemy { get; set; }
        public bool IsEmpty { get; set; }
        public bool IsVisited { get; set; }

        public int X { get; set; }
        public int Y { get; set; }

    }
}
