﻿using LabyrinthDLL;
using System.Collections.Generic;
using System.Web.Http;

namespace GameSolution.Controllers
{
    public class LabyrinthController : ApiController
    {
        public IHttpActionResult Get()
        {
            LabyrinthDLL.Matrix myMatrix = new LabyrinthDLL.Matrix(5, 5);
            Labyrinth myMaze = new Labyrinth();
            ListTransfer result =myMaze.CreateLabyrinth(myMatrix, -100);
            return Ok(result);
        }

       
    }
}
