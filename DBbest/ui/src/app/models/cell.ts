export class Cell{
    IsWall:boolean;
    IsPacman:boolean;
    IsEnemy:boolean;
    IsEmpty:boolean;
    X:number=0;
    Y:number=0;
}