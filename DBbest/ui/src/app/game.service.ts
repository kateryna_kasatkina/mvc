import { Injectable } from '@angular/core';
import { Hero } from './models/hero';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Cell} from './models/cell';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class GameService {

  private gamerName:string='';
  private heroesUrl = 'http://localhost:30057/api/heroes';  // URL to web api
  private labyrintUrl='http://localhost:30057/api/labyrinth';

  constructor(private http: HttpClient) { }

  /** GET heroes from the server */
  public getHeroes (): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl);
  }

  /** POST new hero to the server */
  // public addHero(name: string): void {
  //   // const httpOptions = {
  //   //   headers: new HttpHeaders({
  //   //     'content-type': 'application/json',
  //   //     'charset': 'utf-8'
  //   //   })
  //   // };

  //   let hero = new Hero(0, name, 0);
  //   this.http.post(this.heroesUrl, hero).subscribe();
  // }

 public addHero( gamerName:string):Observable<Hero>
  {
    let newHero:Hero=new Hero( gamerName);
    return this.http.post<Hero>(this.heroesUrl,newHero,{
        headers:new HttpHeaders({
          'content-type': 'application/json'
        })
    })
  }

  /** GET labyrinth from the server */
 public getLabyrinth(): Observable<Cell[][]> {
      return this.http.get(this.labyrintUrl)
      .pipe(
        map((resultResponse: Cell[][]) => {
          return resultResponse;
        })
      );
  }
}