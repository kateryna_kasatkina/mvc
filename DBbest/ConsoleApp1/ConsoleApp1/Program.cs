﻿using System;
using System.Reflection;
using System.Runtime.Remoting;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var DLL = Assembly.LoadFile(@"C:\Users\Катя\Desktop\MazeDLL\bin\Debug\MazeDLL.dll");
            Type[] typesInThisAssembly = DLL.GetTypes();

            Type listTransferType = DLL.GetType("MazeDLL.ListTransfer");
            object listTransfer = Activator.CreateInstance(listTransferType);

            foreach (Type type in typesInThisAssembly)
            {
                if (type.GetInterface("ILabyrinth") != null)
                {
                    Type labyrinthType = DLL.GetType(type.ToString());
                    object labyrinthInstance = Activator.CreateInstance(labyrinthType);
                    listTransfer = labyrinthType.InvokeMember("CreateLabyrinth",
                        BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                        null, labyrinthInstance, null);
                    
                    break;
                }
            }
        }


    }
}
