﻿using System.Collections.Generic;

namespace ConsoleApp1
{
    public class ListTransfer
    {
        public int Rows { get; set; }
        public int Cols { get; set; }
        public List<Cell> List { get; set; }
    }
}
