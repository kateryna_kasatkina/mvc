﻿namespace ConsoleApp1
{
    public class Cell
    {
        // Neighboring cells
        public bool North { get; set; }
        public bool South { get; set; }
        public bool East { get; set; }
        public bool West { get; set; }
    }
}
