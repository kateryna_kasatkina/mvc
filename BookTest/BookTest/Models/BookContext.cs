﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookTest.Models
{
    public class BookContext: DbContext,IBookDataSourse
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Purchase> Purchases { get; set; }

        IQueryable<Book> IBookDataSourse.Books
        {
            get { return Books; }
        }

        IQueryable<Purchase> IBookDataSourse.Purchases
        {
            get { return Purchases; }
        }

        void IBookDataSourse.Save()
        {
            SaveChanges();
        }
    
    }
    
}