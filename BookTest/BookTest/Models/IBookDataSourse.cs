﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookTest.Models
{
    public interface IBookDataSourse
    {
        IQueryable<Book> Books { get; }
        IQueryable<Purchase> Purchases { get; }

        void Save();
    }
}
