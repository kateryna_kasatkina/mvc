﻿using BookTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookTest.Controllers
{
    public class BookController : Controller
    {
        private IBookDataSourse _db;

        public BookController(IBookDataSourse db)
        {
            _db = db;
        }

        // GET: Book
        public ActionResult Index()
        {
            var allBooks = _db.Books;
            return View(allBooks);
        }

        //public ActionResult Detail(int id)
        //{
        //    var model = _db.Books.Single(d => d.Id == id);
        //    return View(model);
        //}

        //[HttpGet]
        //public ActionResult Create(int departmentId)
        //{
        //    var model = new CreateBookViewModel();
        //    model.Id = departmentId;
        //    return View(model);
        //}
    }
}