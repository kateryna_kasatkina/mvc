﻿using SessionTest.Models;
using System;
using System.Web.Mvc;
using System.Web.SessionState;

namespace SessionTest.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(EmpModel obj)
        {
            //store the session from user input and display into the view if session is enabled.  
            Session["Name"] = Convert.ToString(obj.Name);
            return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}