﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IdentityMVC5.Startup))]
namespace IdentityMVC5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
