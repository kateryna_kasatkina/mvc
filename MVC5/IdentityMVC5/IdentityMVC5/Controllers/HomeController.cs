﻿using System.Web.Mvc;

namespace IdentityMVC5.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}