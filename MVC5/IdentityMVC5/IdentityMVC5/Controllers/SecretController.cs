﻿using IdentityMVC5.Helpers;
using System.Web.Mvc;

namespace IdentityMVC5.Controllers
{
    public class SecretController : Controller
    {
        [Authorize]
        public ActionResult SecretQRcode(string email)
        {
            CreateQRbarcode.Run(email);
            return View();
        }
    }
}