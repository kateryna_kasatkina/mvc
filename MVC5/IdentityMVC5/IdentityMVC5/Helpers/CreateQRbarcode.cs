﻿using Aspose.BarCode;
using Aspose.BarCode.Generation;
using System;

namespace IdentityMVC5.Helpers
{
    public static class CreateQRbarcode
    {
        public static void Run(string email = "error")
        {
            BarCodeBuilder builder = new BarCodeBuilder(email, EncodeTypes.QR);
            builder.ImageWidth = 200;
            builder.ImageHeight = 200;
            string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\Images\\code.jpg";
            builder.Save(path, BarCodeImageFormat.Jpeg);
        }
    }
}