namespace ValidationBook.Migrations
{
    using System.Data.Entity.Migrations;
    using ValidationBook.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ValidationBook.Models.BookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ValidationBook.Models.BookContext context)
        {
            context.Books.AddOrUpdate(new Book { Name = "2Book", Author = "2author", Year = 2018 });
            context.Books.AddOrUpdate(new Book { Name = "3Book", Author = "3author", Year = 2019 });
            context.Books.AddOrUpdate(new Book { Name = "4Book", Author = "4author", Year = 2019 });
            context.Books.AddOrUpdate(new Book { Name = "5Book", Author = "5author", Year = 2018 });
            context.Books.AddOrUpdate(new Book { Name = "6Book", Author = "6author", Year = 2018 });
        }
    }
}
