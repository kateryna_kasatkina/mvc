﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ValidationBook.Models;

namespace ValidationBook.MyValidation
{
    public class MyAuthorsAttribute:ValidationAttribute
    {
        //массив для хранения допустимых авторов
        private static string[] myAuthors;
        public MyAuthorsAttribute(string[] Authors)
        {
            myAuthors = Authors;
        }

        public override bool IsValid(object value)
        {
           if(value!=null)
            {
                string strval = value.ToString();
                for (int i = 0; i < myAuthors.Length; i++)
                {
                    if (strval == myAuthors[i])
                        return true;
                }
            }
            return false;
        }
    }

    public class NotAllowedAttribute:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            Book b = value as Book;
            if(b.Name== "bookXXX" && b.Author == "XXX" && b.Year == 1800)
            {
                return false;
            }
            return true;
        }
    }
}