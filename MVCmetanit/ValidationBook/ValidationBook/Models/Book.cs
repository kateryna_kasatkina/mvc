﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ValidationBook.MyValidation;

namespace ValidationBook.Models
{
    [NotAllowedAttribute(ErrorMessage = "Недопустимая книга")]
    public class Book
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [MyAuthors(new string[] { "1author", "2author", "3author", "4author" }, ErrorMessage = "Недопустимый автор")]
        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Год")]
        [Range(1700, 2000, ErrorMessage = "Недопустимый год")]
        public int Year { get; set; }
    }
}