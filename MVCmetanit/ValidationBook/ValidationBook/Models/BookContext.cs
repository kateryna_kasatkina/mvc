﻿using System.Data.Entity;

namespace ValidationBook.Models
{
    public class BookContext:DbContext
    {
        public DbSet<Book> Books { get; set; }
    }
}