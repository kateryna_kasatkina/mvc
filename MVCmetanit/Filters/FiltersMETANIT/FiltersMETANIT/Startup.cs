﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FiltersMETANIT.Startup))]
namespace FiltersMETANIT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
