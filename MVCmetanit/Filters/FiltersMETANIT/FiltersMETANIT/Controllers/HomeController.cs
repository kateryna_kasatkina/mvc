﻿using FiltersMETANIT.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FiltersMETANIT.Controllers
{
    public class HomeController : Controller
    {
        //[MyAuthAttribute]
        //[HandleError(ExceptionType = typeof(System.IndexOutOfRangeException), View = "ExceptionFound")]
        //[IndexException]
        [MyAction]
        public ActionResult Index()
        {
            //int[] mas = new int[2];
            //mas[6] = 4;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}