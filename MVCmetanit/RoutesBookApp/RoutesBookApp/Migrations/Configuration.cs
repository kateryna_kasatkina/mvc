namespace RoutesBookApp.Migrations
{
    using RoutesBookApp.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<RoutesBookApp.Models.BookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RoutesBookApp.Models.BookContext context)
        {
            context.Books.AddOrUpdate(new Book { Name = "First Book", Author = new Author { Name = "First author" } });
            context.Books.AddOrUpdate(new Book { Name = "Second Book", Author = new Author { Name = "Second author" } });
            context.Books.AddOrUpdate(new Book { Name = "Third Book", Author = new Author { Name = "Third author" } });
            context.Books.AddOrUpdate(new Book { Name = "Fouth Book", Author = new Author { Name = "Fouth author" } });
            context.SaveChanges();
        }
    }
}
