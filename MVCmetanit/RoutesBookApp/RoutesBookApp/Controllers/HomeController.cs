﻿using RoutesBookApp.Models;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;//!!!!!!!!!!!!!!!!!!!!!!!!!!

namespace RoutesBookApp.Controllers
{
    public class HomeController : Controller
    {
        BookContext db = new BookContext();

        [Route("book/{id}")]
        public ActionResult GetBook(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Book book = db.Books.Include(b => b.Author).FirstOrDefault(b => b.Id == id);
            if (book == null)
                return HttpNotFound();
            return View(book);
        }

        [Route("book/{id}/author")]
        public ActionResult GetAuthor(int? id)
        {
            if (id == null)
                return HttpNotFound();
            Book book = db.Books.Include(b => b.Author).FirstOrDefault(b => b.Id == id);
            if (book == null)
                return HttpNotFound();
            return View(book.Author);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}