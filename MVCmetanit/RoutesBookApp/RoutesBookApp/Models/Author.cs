﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoutesBookApp.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}