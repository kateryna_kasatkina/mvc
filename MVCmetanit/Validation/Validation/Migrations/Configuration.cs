namespace Validation.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Validation.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Validation.Models.BookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Validation.Models.BookContext context)
        {
            context.Books.Add(new Book { Name = "1Book", Author = "1author", Year = 2014 });
            context.Books.Add(new Book { Name = "2Book", Author = "2author", Year = 2015 });
            context.Books.Add(new Book { Name = "3Book", Author = "3author", Year = 2016 });
            context.Books.Add(new Book { Name = "4Book", Author = "4author", Year = 2017 });
            context.Books.Add(new Book { Name = "5Book", Author = "5author", Year = 2018 });
        }
    }
}
