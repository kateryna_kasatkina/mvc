namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class w : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Role_Role_Id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "Role_Role_Id" });
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        User_id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Role_Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.User_id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Role_Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Role_Id);
            
            CreateIndex("dbo.Users", "Role_Role_Id");
            AddForeignKey("dbo.Users", "Role_Role_Id", "dbo.Roles", "Role_Id");
        }
    }
}
