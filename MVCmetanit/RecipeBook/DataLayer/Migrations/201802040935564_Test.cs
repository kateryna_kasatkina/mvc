namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ingredients", "Recipe_Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.Ingredients", new[] { "Recipe_Recipe_Id" });
            AddColumn("dbo.Ingredients", "Recipe_Recipe_Id1", c => c.Int());
            CreateIndex("dbo.Ingredients", "Recipe_Recipe_Id1");
            AddForeignKey("dbo.Ingredients", "Recipe_Recipe_Id1", "dbo.Recipes", "Recipe_Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ingredients", "Recipe_Recipe_Id1", "dbo.Recipes");
            DropIndex("dbo.Ingredients", new[] { "Recipe_Recipe_Id1" });
            DropColumn("dbo.Ingredients", "Recipe_Recipe_Id1");
            CreateIndex("dbo.Ingredients", "Recipe_Recipe_Id");
            AddForeignKey("dbo.Ingredients", "Recipe_Recipe_Id", "dbo.Recipes", "Recipe_Id");
        }
    }
}
