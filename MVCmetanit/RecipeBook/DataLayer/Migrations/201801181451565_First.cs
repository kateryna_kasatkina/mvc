namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Ingredient_Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Quantity = c.String(nullable: false),
                        Recipe_Recipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Ingredient_Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Recipe_Id)
                .Index(t => t.Recipe_Recipe_Id);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Picture_id = c.Int(nullable: false, identity: true),
                        Recipe_Id = c.Int(nullable: false),
                        RecipePicture = c.Binary(),
                    })
                .PrimaryKey(t => t.Picture_id);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Recipe_Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Recipe_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ingredients", "Recipe_Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.Ingredients", new[] { "Recipe_Recipe_Id" });
            DropTable("dbo.Recipes");
            DropTable("dbo.Pictures");
            DropTable("dbo.Ingredients");
        }
    }
}
