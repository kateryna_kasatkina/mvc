namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Picture : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Recipes", "Picture", c => c.String());
            DropTable("dbo.Pictures");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Picture_id = c.Int(nullable: false, identity: true),
                        Recipe_Id = c.Int(nullable: false),
                        RecipePicture = c.Binary(),
                    })
                .PrimaryKey(t => t.Picture_id);
            
            DropColumn("dbo.Recipes", "Picture");
        }
    }
}
