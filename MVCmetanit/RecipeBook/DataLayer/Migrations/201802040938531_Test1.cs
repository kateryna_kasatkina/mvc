namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Ingredients", "Recipe_Recipe_Id");
            RenameColumn(table: "dbo.Ingredients", name: "Recipe_Recipe_Id1", newName: "Recipe_Recipe_Id");
            RenameIndex(table: "dbo.Ingredients", name: "IX_Recipe_Recipe_Id1", newName: "IX_Recipe_Recipe_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Ingredients", name: "IX_Recipe_Recipe_Id", newName: "IX_Recipe_Recipe_Id1");
            RenameColumn(table: "dbo.Ingredients", name: "Recipe_Recipe_Id", newName: "Recipe_Recipe_Id1");
            AddColumn("dbo.Ingredients", "Recipe_Recipe_Id", c => c.Int());
        }
    }
}
