namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserProfile", newName: "Users");
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Role_Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Role_Id);
            
            AddColumn("dbo.Users", "Role_Role_Id", c => c.Int());
            CreateIndex("dbo.Users", "Role_Role_Id");
            AddForeignKey("dbo.Users", "Role_Role_Id", "dbo.Roles", "Role_Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Role_Role_Id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "Role_Role_Id" });
            DropColumn("dbo.Users", "Role_Role_Id");
            DropTable("dbo.Roles");
            RenameTable(name: "dbo.Users", newName: "UserProfile");
        }
    }
}
