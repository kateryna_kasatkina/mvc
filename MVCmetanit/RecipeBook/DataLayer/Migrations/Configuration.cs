namespace DataLayer.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DataLayer.MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataLayer.MyContext context)
        {
            context.recipes.AddOrUpdate(new Recipe
            {
                Name = "���� � ������ � ����",
                Description = "��������� ���� � ������� �������, ������� ������, �������� ������ ���, �������� �����",
                Ingredients = new List<Ingredient>
                {
                    new Ingredient{Name="����", Quantity="2 ��"},
                    new Ingredient{Name="�����", Quantity="15 ��"},
                    new Ingredient{Name="���", Quantity="5 ��"},
                    new Ingredient{Name="����", Quantity="400 ��"},
                    new Ingredient{Name="�����", Quantity="20 ��"},
                },
                Picture="./Content/Images/2.jpg"
            });

            context.recipes.AddOrUpdate(new Recipe
            {
                Name = "��������� � ��������",
                Description = "�������� ����, �������� �������, ���� ������� ������, ������ ��������� ���������� �������",
                Ingredients = new List<Ingredient>
                {
                    new Ingredient{Name="����", Quantity="1 ��"},
                    new Ingredient{Name="�����", Quantity="15 ��"},
                    new Ingredient{Name="�������", Quantity="50 ��"},
                },
                Picture = "./Content/Images/3.jpg"
            });

            for (int i = 0; i < 100; i++)
            {
                context.recipes.AddOrUpdate(new Recipe
                {
                    Name = i.ToString(),
                    Description = "some description",
                    Ingredients = new List<Ingredient>(),
                    Picture = "./Content/Images/3.jpg"
                });
            }
        }
    }
}
