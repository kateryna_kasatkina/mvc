﻿using System.Data.Entity;

namespace DataLayer
{
    public class MyContext:DbContext
    {
        public MyContext() : base("Recipebook") { }
        public DbSet<Ingredient> ingredients { get; set; }
        public DbSet<Recipe> recipes { get; set; }
       

    }
}
 