﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer
{
    public class Recipe
    {
        [Key]
        public int Recipe_Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual List<Ingredient> Ingredients { get; set; }

        public string Picture { get; set; }

        
    }
}
