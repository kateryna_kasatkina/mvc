﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer
{
    public class Ingredient
    {
        [Key]
        public int Ingredient_Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Quantity { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}

