﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecipeBook.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        MyContext db = new MyContext();

        public ActionResult Index()
        {
            IEnumerable<Recipe> recipes = db.recipes;
            ViewBag.Recipes = recipes;
            return View();
        }

        public ActionResult GetSingle(int Id)
        {
            IEnumerable<Recipe> recipe = db.recipes.Where(r => r.Recipe_Id == Id);
            ViewBag.Recipe = recipe;
            return View();
        }
    }
}