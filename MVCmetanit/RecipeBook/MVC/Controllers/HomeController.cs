﻿using DataLayer;
using MVC.Models;
using PagedList;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        MyContext _db = new MyContext();

        public ActionResult Autocomplete(string term)
        {
            var model = _db.recipes
                .Where(r => r.Name.StartsWith(term))
                .Select(r => new//because object should have label or valeu property
                {
                    label = r.Name
                });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[ChildActionOnly]
        //[OutputCache(Duration=60)]
        //public ActionResult SayHello()
        //{
        //    return Content("Hello");
        //}

        [OutputCache(Duration =360,VaryByHeader ="X-Requested-With",Location =OutputCacheLocation.ServerAndClient)]
        public async Task<ActionResult> Index(string searchTerm=null,int page=1)
        {
           var model= _db.recipes
               .OrderByDescending(r=>r.Name)
               .Where(r => searchTerm == null || r.Name.StartsWith(searchTerm))
               .Select(r => new RecipeListViewModel
               {
                   Recipe_Id=r.Recipe_Id,
                   Name=r.Name,
                   Picture=r.Picture,
                   Description=r.Description,
                   Ingredients=r.Ingredients
               })
               .ToPagedList(page,3);
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Recipes",model);
            }
            return View(model);
            
        }

        protected override void Dispose(bool disposing)
        {
            if(_db!=null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}