﻿using DataLayer;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class RecipeController : Controller
    {
        MyContext _db = new MyContext();

        // GET: Recipe
        public ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult Search(string searchTerm)
        //{
        //    var selected = _db.recipes.Where(r => r.Name.StartsWith(searchTerm))
        //        .Select(r => r)
        //        .ToList();
        //    return View("~/Views/Home/Index.cshtml", selected);
        //}

        public ActionResult GetIngredients()
        {
            var ingredients = _db.ingredients.Include(i => i.Recipe);
            return View(ingredients.ToList());
        }


        public ActionResult Details(int id)
        {
            Recipe recipe = _db.recipes.Where(r => r.Recipe_Id == id).FirstOrDefault();
            if(recipe==null)
            {
                ViewBag.message = "No recipe with such id";
                return View("View");
            }
            return View(recipe);
        }

        // GET: Recipe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Recipe/Create
        [HttpPost]
        public ActionResult Create(Recipe recipe)
        {
            if(ModelState.IsValid)
            {
                _db.recipes.Add(recipe);
                _db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = "Non Valid";
            return View();
        }

        // GET: Recipe/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Recipe recipe = _db.recipes.Find(id);
            if (recipe != null)
            {
                return View(recipe);
            }
            return HttpNotFound();
        }

        // POST: Recipe/Edit/5
        [HttpPost]
        public ActionResult Edit(Recipe recipe)
        {
                _db.Entry(recipe).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index","Home");
        }

        // GET: Recipe/Delete/5
        public ActionResult Delete(int id)
        {
            Recipe recipe = _db.recipes.Find(id);
            if(recipe==null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        // POST: Recipe/Delete/5
        [HttpPost,ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipe b = _db.recipes.Find(id);
            if(b==null)
            {
                return HttpNotFound();
            }
            _db.recipes.Remove(b);
            _db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
