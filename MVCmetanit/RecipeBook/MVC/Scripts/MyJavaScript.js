﻿$(function () {
    var ajaxFormSubmit = function () {
        var $from = $(this);
        var options =
            {
                url: $from.attr("action"),
                type: $from.attr("method"),
                data: $from.serialize()  // ALEX: changed $form to $from
            };
        $.ajax(options).done(function (data) {
            var $target = $($from.attr("data-otf-target"));
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            $newHtml.effect("higlight");
        });

        return false;
    };

    var submitAutocompleteForm = function (event, ui)
    {
        var $input = $(this);
        $input.val(ui.item.label);

        var $form = $input.parents("form:first");
        $form.submit();
    }

    var createAutocomplete = function ()
    {
        var $input = $(this);

        var options = {
            source: $input.attr("data-otf-autocomplete"),
            select: submitAutocompleteForm
        };
        //console.log(options);   // ALEX: changed console.log(source) to console.log(options)
        $input.autocomplete(options);
    }

    var getPage = function ()
    {
        var $a = $(this);
        
        var options = {
            url: $a.attr("href"),
            data: $("form").serialize(),
            type:"get"
        };

        $.ajax(options).done(function (data) {
            var target = $a.parents("div.pagedList").attr("data-otf-target");
            $(target).replaceWith(data);
        });
        return false;
    }

    $("form[data-otf-ajax='true']").submit(ajaxFormSubmit);
    $("input[data-otf-autocomplete]").each(createAutocomplete);
  
    $(".main-content").on("click", ".pagedList a", getPage);
});