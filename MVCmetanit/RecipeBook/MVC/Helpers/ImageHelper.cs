﻿using DataLayer;
using MVC.Models;
using System.Web.Mvc;

namespace MVC.Helpers
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper html,string src,string alt)
        {
            TagBuilder img = new TagBuilder("img");
            img.MergeAttribute("src", src);
            img.MergeAttribute("alt", alt);
            img.MergeAttribute("width", "200");
            img.MergeAttribute("height", "200");
            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }



        public static MvcHtmlString ImageRecipe(this HtmlHelper html, Recipe recipe)
        {
            TagBuilder img = new TagBuilder("img");
            img.MergeAttribute("src", recipe.Picture);
            img.MergeAttribute("alt", "picture");
            img.MergeAttribute("width", "100");
            img.MergeAttribute("height", "100");
            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString ImageRecipeViewModel(this HtmlHelper html, RecipeListViewModel recipe)
        {
            TagBuilder img = new TagBuilder("img");
            img.MergeAttribute("src", recipe.Picture);
            img.MergeAttribute("alt", "picture");
            img.MergeAttribute("width", "100");
            img.MergeAttribute("height", "100");
            return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
        }
    }
}