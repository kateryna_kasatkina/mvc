﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Helpers
{
    public static class ListHelper
    {
        public static MvcHtmlString CreateList(this HtmlHelper html,string[] items, object htmlAttributes=null)
        {
            TagBuilder ul = new TagBuilder("ul");
            foreach (string item in items)
            {
                TagBuilder li = new TagBuilder("li");
                li.SetInnerText(item);
                ul.InnerHtml += li.ToString();

            }
            ul.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            return new MvcHtmlString(ul.ToString());
        }

    }


    public static class ListHelperL
    {
        public static MvcHtmlString CreateList(this HtmlHelper html,List<Ingredient> ingredients, object htmlAttributes = null)
        {
            TagBuilder ul = new TagBuilder("ul");
            foreach (Ingredient item in ingredients)
            {
                TagBuilder li = new TagBuilder("li");
                li.SetInnerText(item.Name);
                ul.InnerHtml += li.ToString();

            }
            ul.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            return new MvcHtmlString(ul.ToString());
        }

    }
}