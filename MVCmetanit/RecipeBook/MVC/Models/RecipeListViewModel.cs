﻿using DataLayer;
using System.Collections.Generic;

namespace MVC.Models
{
    public class RecipeListViewModel
    {
        public int Recipe_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public  List<Ingredient> Ingredients { get; set; }

        public string Picture { get; set; }

    }
}