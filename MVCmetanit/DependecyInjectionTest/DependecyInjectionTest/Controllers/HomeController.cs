﻿using DependecyInjectionTest.Models;
using Ninject;
using System.Web.Mvc;

namespace DependecyInjectionTest.Controllers
{
    public class HomeController : Controller
    {
        IRepository repo;//inversion of control (IoC)

        public HomeController(IRepository r)
        {
            //Чтобы управлять зависимостями через Ninject, 
            //вначале надо создать объект Ninject.IKernel с 
            //помощью встроенной реализации этого интерфейса - класса StandardKernel
            IKernel ninjectKernel = new StandardKernel();

            //Далее нужно установить отношения между интерфейсами и их реализациями:
            ninjectKernel.Bind<IRepository>().To<BookRepository>();

            //Данное выражение указывает, что объекты IRepository должны будут рассматриваться как BookRepository.
            //И в конце создается объект интерфейса через метод Get:
            repo = ninjectKernel.Get<IRepository>();

            //Поскольку выше мы установили сопоставление между
            //IRepository и BookRepository, то метод ninjectKernel.Get<IRepository>() 
            //будет создавать экземпляр класса BookRepository.
        }

        public ActionResult Index()
        {
            return View(repo.List());
        }
    }
}