﻿using System.Collections.Generic;

namespace DependecyInjectionTest.Models
{
    public interface IRepository
    {
        void Save(Book b);
        IEnumerable<Book> List();
        Book Get(int id);
    }
}