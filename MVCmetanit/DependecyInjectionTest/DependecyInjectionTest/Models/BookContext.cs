﻿using System.Data.Entity;

namespace DependecyInjectionTest.Models
{
    public class BookContext : DbContext
    {
        public BookContext() : base("DefaultConnection") { }

        public DbSet<Book> Books { get; set; }
    }
}