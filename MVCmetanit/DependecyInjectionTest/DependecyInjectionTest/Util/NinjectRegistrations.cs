﻿using DependecyInjectionTest.Models;
using Ninject.Modules;

namespace DependecyInjectionTest.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            //Bind<IRepository>().To<BookRepository>()
            //    .WithConstructorArgument("context", new BookContext());


            Bind<IRepository>().To<BookRepository>()
                .WithPropertyValue("Context", new BookContext());
        }
    }
}