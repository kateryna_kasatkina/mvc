namespace FilteringData.Migrations
{
    using FilteringData.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FilteringData.Models.SoccerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FilteringData.Models.SoccerContext context)
        {
            Team t1 = new Team { Name = "���������" };
            Team t2 = new Team { Name = "���� ������" };
            context.Teams.Add(t1);
            context.Teams.Add(t2);
            context.SaveChanges();
            Player pl1 = new Player { Name = "�������", Age = 31, Position = "����������", Team = t2 };
            Player pl2 = new Player { Name = "�����", Age = 28, Position = "����������", Team = t1 };
            Player pl3 = new Player { Name = "����", Age = 34, Position = "������������", Team = t1 };
            context.Players.AddRange(new List<Player> { pl1, pl2, pl3 });
            context.SaveChanges();
        }
    }
}
