﻿using System.Data.Entity;

namespace FilteringData.Models
{
    public class SoccerContext : DbContext
    {
        public SoccerContext() : base("SoccerContext")
        { this.Configuration.LazyLoadingEnabled = false; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
    }

   
}