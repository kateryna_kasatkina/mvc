﻿using System.Collections.Generic;

namespace Paging.Models
{/// <summary>
/// Так как вместе с данными моделей телефонов потребуется также 
/// использовать в представлении модель пагинации,
/// то они инкапсулируются классом IndexViewModel.
/// </summary>
    public class IndexViewModel
    {
        public IEnumerable<Phone> Phones { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}