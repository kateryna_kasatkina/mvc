﻿using MyMenue.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MyMenue.Controllers
{
    public class HomeController : Controller
    {
        ApplicationContext db = new ApplicationContext();

        public ActionResult Menu()
        {
            List<MenuItem> menuItems = db.MenuItems.ToList();

            return PartialView(menuItems);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}