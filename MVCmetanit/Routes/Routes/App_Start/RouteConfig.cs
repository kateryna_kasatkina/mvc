﻿using Routes.RouteHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Routes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //Route newRoute = new Route("{controller}/{action}", new MyRouteHandler());
            //routes.Add(newRoute);
            // routes.MapRoute(name: "Default", url: "UA/{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });

            routes.MapMvcAttributeRoutes();


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


            



        }
    }
}
