﻿using System.Web;

namespace Routes.RouteHandler
{
    internal class MyHttpHandler : IHttpHandler
    {
        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Write("Инопланетное послание : Привет мир!");
        }
    }
}