﻿using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using SignalRChat.Models;

namespace SignalRChat
{
    public class ChatHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }


    }
}