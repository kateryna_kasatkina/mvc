﻿using LifeCycle.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LifeCycle
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add(new Route("home/about", new SampleRouteHandler()));

            routes.MapRoute(
                name: "default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
            );

            //Route myRoute = new Route("{controller}/{action}/{id}",
            //    new RouteValueDictionary { { "controller", "Home" }, { "action", "Index" }, { "id", "1" } },
            //    new MvcRouteHandler());
            //routes.Add(myRoute);
        }
    }
}
