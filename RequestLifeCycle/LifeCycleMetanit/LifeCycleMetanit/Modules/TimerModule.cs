﻿using System;
using System.Diagnostics;
using System.Web;

namespace LifeCycleMetanit.Modules
{
    public class RequestTimerEventArgs : EventArgs
    {
        public float Duration { get; set; }
    }

    public class TimerModule : IHttpModule
    {
        private Stopwatch timer;
        public event EventHandler<RequestTimerEventArgs> RequestTimed;

        public void Dispose()
        {}

        public void Init(HttpApplication app)
        {
            app.BeginRequest += HandleBeginRequest;
            app.EndRequest += HandleEndRequest;
        }

        private void HandleEndRequest(object src, EventArgs args)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Write(string.Format("<div style='color:red;'>Время обработки запроса: {0:F5} секунд</div>",
                ((float)timer.ElapsedTicks) / Stopwatch.Frequency));

            if (RequestTimed != null)
            {
                RequestTimed(this,
                new RequestTimerEventArgs { Duration = duration });
            }
        }

        private void HandleBeginRequest(object src, EventArgs args)
        {
            timer = Stopwatch.StartNew();
        }
    }
}