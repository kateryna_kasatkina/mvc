﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LifeCycleMetanit.Controllers
{
    public class HomeController : Controller
    {
        public string Index()
        {
            List<string> events = HttpContext.Application["events"] as List<string>;
            string result = "<ul>";
            foreach (string e in events)
                result += "<li>" + e + "</li>";
            result += "</ul>";
            return result;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}