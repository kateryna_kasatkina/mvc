﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestTaskDaxx.Startup))]
namespace TestTaskDaxx
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
